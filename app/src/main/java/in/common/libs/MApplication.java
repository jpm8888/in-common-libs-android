package in.common.libs;

import android.app.Application;

import in.common.libs.db.AppDatabase;
import in.common.libs.retrofit.ApiClient;
import in.common.libs.retrofit.ApiInterface;
import in.common.libs.utils.Toaster;
import in.common.libs.utils.Utils;

public class MApplication extends Application {

    ApiClient apiClient;
    AppDatabase appDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        Toaster.setContext(this);
        Utils.setContext(this);
    }

    public ApiClient getApiClient() {
        if (apiClient == null) apiClient = new ApiClient(this);
        return apiClient;
    }

    public ApiInterface getApiInterface(){
        return getApiClient().getApiInterface();
    }

    public AppDatabase getAppDatabase() {
        if (appDatabase == null) appDatabase = AppDatabase.getInstance(this);
        return appDatabase;
    }
}
