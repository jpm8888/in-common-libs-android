package in.common.libs.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class AdapterEmpty extends RecyclerView.Adapter<AdapterEmpty.MyViewHolder> {

    private final Activity activity;
    private List<String> items;
    private int layout_id = 1; //replace with R.layout.item_xyz


    public AdapterEmpty(Activity activity, List<String> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(layout_id, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        final Result item = items.get(position);

//        holder.textViewName.setText(item.getName());
//        holder.textViewAddress.setText("NA");
//        holder.textViewDistance.setText(distance + " Kms");
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View view;
//        public TextView textViewName, textViewAddress, textViewDistance;

        public MyViewHolder(View v) {
            super(v);
            this.view = v;
//            textViewName = v.findViewById(R.id.textViewName);
        }
    }

}