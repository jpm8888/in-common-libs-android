package in.common.libs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


/**
 * Created by jpm on 05/06/18.
 */

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    public Fragment currentFragment;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        //mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
    }

    public void showActionBar(boolean show) {
        if (getSupportActionBar() != null) {
            if (show) getSupportActionBar().show();
            else getSupportActionBar().hide();
        }
    }


    public void showDialog() {
        if (mProgressDialog != null) this.mProgressDialog.show();
    }


    public void setFragment(Fragment fragment, String title) {
        if (fragment != null) {
            this.currentFragment = fragment;
            FragmentTransaction trasTransaction = getSupportFragmentManager().beginTransaction();
            trasTransaction.replace(R.id.container, currentFragment, title);

            setTitle(title);

            trasTransaction.addToBackStack(null);
            trasTransaction.commit();
        }
    }

    public void setTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    public void hideDialog() {
        this.mProgressDialog.hide();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showKeyboard(EditText editText) {
        editText.requestFocus();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public void onBackPressed() {
        int counter = getSupportFragmentManager().getBackStackEntryCount();
        if (counter > 0) {
            if (counter == 1) {
                finish();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            finish();
        }
    }

}
