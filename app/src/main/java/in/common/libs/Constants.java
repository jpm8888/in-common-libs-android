package in.common.libs;

public class Constants {
    public static final String BASE_URL = "https://someurl";
    public static final long SPLASH_TIME = 3000;
    public static final boolean DEBUG = true;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "db_name";
    public static final String APP_NAME = "APP NAME";
    public static final String PUBLISHER_NAME = "ACCOUNT_NAME";
}
