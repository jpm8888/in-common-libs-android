package in.common.libs.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import in.common.libs.Constants;
import in.common.libs.db.dao.Dao;



//entities = TableDemo.class, TableDemo1.class
@Database(entities = {}, version = Constants.DATABASE_VERSION)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;
    public abstract Dao dao();

    //if you want to add migrations
    //.addMigrations(AppDatabase.MIGRATION_1_2)
    public static AppDatabase getInstance(Context context){
        if (instance == null) instance = Room.databaseBuilder(context, AppDatabase.class, Constants.DATABASE_NAME).build();
        return instance;
    }

    //    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//
//        }
//    };

}
