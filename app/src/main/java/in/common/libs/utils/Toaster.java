package in.common.libs.utils;

import android.content.Context;
import android.widget.Toast;

public class Toaster {
    private static Context context;

    //use application to initialize the context.
    public static void setContext(Context context) {
        Toaster.context = context;
    }

    public static void makeToast(String msg) {
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }
}
