package in.common.libs.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Uri;

import in.common.libs.Constants;

public class Utils {
    private static Context context;
    public static void setContext(Context context) {
        Utils.context = context;
    }

    public static void rateThisApp() {
        if (isNetConnected()) {
            String packageName = context.getPackageName();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } else {
            Toaster.makeToast("Please enable wifi or data from settings");
        }
    }

    public static boolean isNetConnected() {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected();
    }

    public static void shareApp() {
        if (isNetConnected()) {
            Resources res = context.getResources();
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.APP_NAME);

            String sharingText = "You should check this out, it's great - ";
            sharingIntent.putExtra(Intent.EXTRA_TEXT, sharingText + "https://play.google.com/store/apps/details?id=" + context.getPackageName());

            Intent i = Intent.createChooser(sharingIntent, "Share via");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } else {
            Toaster.makeToast("Please enable wifi or data from settings");
        }
    }

    public static void openPlayStore(String packageName) {
        if (isNetConnected()) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else {
            Toaster.makeToast("Please enable wifi or data from settings");
        }
    }


    public static void moreApps() {
        if (isNetConnected()) {
            Intent pubPage = new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:" + Constants.PUBLISHER_NAME));
            pubPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(pubPage);
        } else {
            Toaster.makeToast("Please enable wifi or data from settings");
        }
    }

    public static void feedback(String feedbackMail) {
        if (isNetConnected()) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.putExtra(Intent.EXTRA_EMAIL,
                    new String[]{feedbackMail});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                    "Feedback for " + Constants.APP_NAME);
            emailIntent.putExtra(Intent.EXTRA_TEXT,
                    "Write your feedback");
            context.startActivity(Intent.createChooser(emailIntent, "Send mail...").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else {
            Toaster.makeToast("Please enable wifi or data from settings");
        }
    }

    public static String getAppLink(Context context) {
        String appLink = "\n market://details?id=" + context.getPackageName();
        return appLink;
    }

}
