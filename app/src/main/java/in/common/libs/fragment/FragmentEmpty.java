package in.common.libs.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.common.libs.BaseActivity;

public class FragmentEmpty extends Fragment{

    public static final String TITLE = "Contacts";
    public static final int ID_LAYOUT = 1; //replace it with R.layout.layout_name

    private View view;
    private BaseActivity activity;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (BaseActivity) getActivity();
        view = inflater.inflate(ID_LAYOUT, container, false);
        init();
        return view;
    }


    private void init() {

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(TITLE);
    }
}