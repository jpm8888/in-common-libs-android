## Read Me First
### Version : 1

#### Layout in RecyclerView 
```sh
xml code : 

<android.support.v7.widget.RecyclerView
        android:id="@+id/recyclerView"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
    </android.support.v7.widget.RecyclerView>

java code :

private void populateItems(List<ModelItems> items) {
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        Adapter mAdapter = new Adapter(activity, items);
        recyclerView.setAdapter(mAdapter);
}
    
    
```

#### Layout in CardView 
```sh
xml code : 

<android.support.v7.widget.CardView xmlns:card_view="http://schemas.android.com/apk/res-auto"
        android:id="@+id/card_view"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        card_view:cardBackgroundColor="#f5f5f5"
        card_view:cardCornerRadius="4dp"
        card_view:cardMaxElevation="10dp"
        card_view:cardUseCompatPadding="true">
        
</android.support.v7.widget.CardView>
```

#### Layout in EditText 
```sh
xml code : 

<android.support.design.widget.TextInputLayout
        android:layout_width="match_parent"
            android:layout_height="wrap_content">
            <EditText
                android:id="@+id/editTextName"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:drawableRight="@android:drawable/ic_menu_search"
                android:hint="Enter Number"
                android:maxLength="10"
                android:imeOptions="actionSearch"
                android:digits="0123456789"
                android:inputType="phone"
                android:singleLine="true"
                android:textColorHint="#a2a2a2" />
</android.support.design.widget.TextInputLayout>
```

